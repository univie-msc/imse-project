package org.gapsy.community.controllers;

import org.gapsy.community.exceptions.ForbiddenException;
import org.gapsy.community.exceptions.NotFoundException;
import org.gapsy.community.models.Group;
import org.gapsy.community.models.Story;
import org.gapsy.community.models.User;
import org.gapsy.community.repositories.GroupRepository;
import org.gapsy.community.repositories.StoryRepository;
import org.gapsy.community.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(path="/group")
public class GroupController extends BaseController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StoryRepository storyRepository;

    @GetMapping("all")
    public ModelAndView all(HttpServletRequest request) {
        Iterable<Group> groups = groupRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("group-list");
        modelAndView.addObject("groups", groups);
        modelAndView.addObject("currentUser", getCurrentUser(request));

        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView one(@PathVariable Integer id, HttpServletRequest request) {
        Group group = groupRepository.findById(id).orElseThrow(() -> new NotFoundException());

        if (!group.isMember(getCurrentUser(request))) {
            throw new ForbiddenException();
        }

        ModelAndView modelAndView = new ModelAndView("group");
        modelAndView.addObject("group", group);

        return modelAndView;
    }

    @GetMapping("/{id}/story/add")
    public ModelAndView addStory(@PathVariable Integer id) {
        Group group = groupRepository.findById(id).orElseThrow(() -> new NotFoundException());

        ModelAndView modelAndView = new ModelAndView("add-story");
        modelAndView.addObject("group", group);
        modelAndView.addObject("story", new Story());

        return modelAndView;
    }

    @PostMapping("/{groupId}/story/save")
    public String saveStory(@PathVariable Integer groupId, @Valid Story story, HttpServletRequest request) {
        Group group = groupRepository.findById(groupId).orElseThrow(() -> new NotFoundException());

        User user = getCurrentUser(request);

        if (!group.isMember(user)) {
            throw new ForbiddenException();
        }

        // TODO save user_id

        story = storyRepository.save(story);
        group.addStory(story);
        groupRepository.save(group);

        return "redirect:/group/" + group.getId();
    }

    @RequestMapping(value = "join", method = RequestMethod.POST)
    public String join(@RequestParam Integer id, HttpServletRequest request) {
        Group group = groupRepository.findById(id).orElseThrow(() -> new NotFoundException());

        User user = getCurrentUser(request);

        if (!group.isMember(user)) {
            user.addGroup(group);
            userRepository.save(user);
        }

        return "redirect:" + group.getId();
    }

    @RequestMapping("leave")
    public String leave() {
        return "";
    }

    @RequestMapping("post-story")
    public String postStory() {
        return "post-story";
    }
}
