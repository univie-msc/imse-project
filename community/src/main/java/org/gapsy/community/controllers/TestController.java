package org.gapsy.community.controllers;

import org.gapsy.community.exceptions.ForbiddenException;
import org.gapsy.community.exceptions.NotFoundException;
import org.gapsy.community.models.Group;
import org.gapsy.community.models.Story;
import org.gapsy.community.models.User;
import org.gapsy.community.repositories.GroupRepository;
import org.gapsy.community.repositories.StoryRepository;
import org.gapsy.community.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/test")
public class TestController extends BaseController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StoryRepository storyRepository;

    @GetMapping("/add-story")
    public String addStory() {
        /*Group group = groupRepository.findById(2).get();
        User user = userRepository.findById(17).get();
        Story story = new Story();
        story.setTitle("Title");
        story.setContent("Content");

        //story.addUser(user);
        story = storyRepository.save(story);
        group.addStory(story);
        groupRepository.save(group);*/

        return "TEST";
    }





    @GetMapping("/join-group")
    public String joinGroup() {
        /*User user = userRepository.findById(17).get();
        Group group = groupRepository.findById(2).get();

        //group.addUser(user);
        //groupRepository.save(group);

        user.addGroup(group);
        userRepository.save(user);*/

        return "TEST";
    }
}
