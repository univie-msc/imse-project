package org.gapsy.community.controllers;

import org.gapsy.community.models.ServiceProvider;
import org.gapsy.community.models.Student;
import org.gapsy.community.repositories.ServiceProviderRepository;
import org.gapsy.community.repositories.StudentRepository;
import org.gapsy.community.repositories.UserRepository;
import org.gapsy.community.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.security.SecureRandom;

@Controller
@RequestMapping(path="/user")
public class UserController extends BaseController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ServiceProviderRepository serviceProviderRepository;

    @GetMapping(path="/all")
    public ModelAndView all() {
        Iterable<User> users = userRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("user-list");
        modelAndView.addObject("users", users);

        return modelAndView;
    }
}
