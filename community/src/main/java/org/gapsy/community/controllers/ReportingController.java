package org.gapsy.community.controllers;

import org.gapsy.community.models.Opportunity;
import org.gapsy.community.models.Student;
import org.gapsy.community.repositories.OpportunityRepository;
import org.gapsy.community.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

@Controller
@RequestMapping(path="/reporting")
public class ReportingController extends BaseController {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private OpportunityRepository opportunityRepository;

    @GetMapping("/student-applications")
    public ModelAndView studentApplications() {
        Iterable<Opportunity> opportunities = opportunityRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("student-applications");
        modelAndView.addObject("opportunities", opportunities);

        return modelAndView;
    }
}
