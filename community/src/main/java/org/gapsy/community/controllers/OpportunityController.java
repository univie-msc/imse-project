package org.gapsy.community.controllers;

import org.gapsy.community.exceptions.ForbiddenException;
import org.gapsy.community.exceptions.NotFoundException;
import org.gapsy.community.models.Opportunity;
import org.gapsy.community.models.ServiceProvider;
import org.gapsy.community.models.Student;
import org.gapsy.community.models.User;
import org.gapsy.community.repositories.OpportunityRepository;
import org.gapsy.community.repositories.ServiceProviderRepository;
import org.gapsy.community.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(path="/opportunity")
public class OpportunityController extends BaseController {
    @Autowired
    private OpportunityRepository opportunityRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ServiceProviderRepository serviceProviderRepository;

    @GetMapping("/all")
    public ModelAndView all(HttpServletRequest request) {
        Iterable<Opportunity> opportunities = opportunityRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("opportunity-list");
        modelAndView.addObject("opportunities", opportunities);
        modelAndView.addObject("currentUser", getCurrentUser(request));

        return modelAndView;
    }

    @RequestMapping(value="/advertise", method = RequestMethod.GET)
    public ModelAndView advertise() {
        ModelAndView modelAndView = new ModelAndView();
        Opportunity opportunity = new Opportunity();
        modelAndView.addObject("opportunity", opportunity);
        modelAndView.setViewName("advertise");

        return modelAndView;
    }

    @RequestMapping(value="/advertise", method = RequestMethod.POST)
    public String save(@Valid Opportunity opportunity, HttpServletRequest request) {
        ServiceProvider serviceProvider = getCurrentUser(request).getServiceProvider();

        //opportunity.addServiceProvider(serviceProvider);
        opportunityRepository.save(opportunity);

        serviceProvider.addOpportunity(opportunity);
        serviceProviderRepository.save(serviceProvider);

        return "redirect:all";
    }

    @RequestMapping(value = "apply", method = RequestMethod.POST)
    public String apply(@RequestParam Integer id, HttpServletRequest request) {
        Opportunity opportunity = opportunityRepository.findById(id).orElseThrow(() -> new NotFoundException());

        User user = getCurrentUser(request);
        Student student = user.getStudent();

        if (student == null) {
            throw new ForbiddenException();
        }

        if (!opportunity.hasApplied(student)) {
            student.addOpportunity(opportunity);
            studentRepository.save(student);
        }

        return "redirect:all";
    }
}
