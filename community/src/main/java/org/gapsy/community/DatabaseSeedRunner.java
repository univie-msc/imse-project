package org.gapsy.community;

import org.gapsy.community.models.*;
import org.gapsy.community.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DatabaseSeedRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseSeedRunner.class);

    private List<String> users = Arrays.asList(
        "1;admin@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Admin;Admin;ADMIN",
        "2;student@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Student;Student;STUDENT",
        "3;sp@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Service;Provider;SERVICE_PROVIDER",
        "4;john.doe@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;John;Doe;STUDENT",
        "5;jane.doe@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Jane;Doe;STUDENT",
        "6;andrew@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Andrew;Smith;STUDENT",
        "7;laura@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Laura;Baker;STUDENT",
        "8;isa@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Isabella;Swanson;STUDENT",
        "9;emilia@example.com;$2a$10$Cro/yTREVkxiC3wBMUFCNeEskFFy9nTWcspLzY/Mc4Gx94XulXOnO;Emilia;Morrison;SERVICE_PROVIDER"
    );

    private List<String> groups = Arrays.asList(
        "1;All volunteers' group;Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "2;I <3 Austria (gap year in Austria);Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "3;Wiener Schnitzel bakers' charity club;Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "4;Volunteering on Mars;Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "5;Alumni group;Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    );

    private List<String> opportunities = Arrays.asList(
        "1;Volunteer on the Moon!;Lorem ipsum dolor sit amet, consectetur adipiscing elit.;The Moon;2020-01-01;2020-12-31",
        "2;Exchange year on Bali;Lorem ipsum dolor sit amet, consectetur adipiscing elit.;Bali (Indonesia);2020-01-01;2020-12-31"
    );

    private List<String> memberships = Arrays.asList(
        "1;1",
        "1;2",
        "1;3",
        "2;1",
        "2;2",
        "3;1"
    );

    private List<String> stories = Arrays.asList(
        "1;Lorem ipsum;Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "2;Facebook claims its new chatbot beats Google’s as the best in the world;For all the progress that chatbots and virtual assistants have made, they’re still terrible conversationalists. Most are highly task-oriented: you make a demand and they comply. Some are highly frustrating: they never seem to get what you’re looking for. Others are awfully boring: they lack the charm of a human companion. It’s fine when you’re only looking to set a timer. But as these bots become increasingly popular as interfaces for everything from retail to health care to financial services, the inadequacies only grow more apparent."
    );

    private List<String> sharings = Arrays.asList(
        ";1;1",
        ";1;2"
    );

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdministratorRepository administratorRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ServiceProviderRepository serviceProviderRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private OpportunityRepository opportunityRepository;
    @Autowired
    private StoryRepository storyRepository;

    private void processUser(List<String> values) {
        User user = new User();
        user.setId(Integer.parseInt(values.get(0)));
        user.setEmail(values.get(1));
        user.setPassword(values.get(2));
        user.setFirstname(values.get(3));
        user.setLastname(values.get(4));
        user = userRepository.save(user);

        switch (values.get(5)) {
            case "ADMIN":
                Administrator administrator = new Administrator();
                administrator.setUserId(user.getId());
                administrator.setLevel(0);
                administrator.setEmergencyContactPhone("+36201234567");
                administratorRepository.save(administrator);
                break;
            case "STUDENT":
                Student student = new Student();
                student.setUserId(user.getId());
                student.setSchoolName("University of Vienna");
                student.setExpectedGraduationDate("2020-01-01");
                studentRepository.save(student);
                break;
            case "SERVICE_PROVIDER":
                ServiceProvider serviceProvider = new ServiceProvider();
                serviceProvider.setUserId(user.getId());
                serviceProvider.setCompanyName("ABC Ltd.");
                serviceProvider.setBusinessAddress("Over the rainbow");
                serviceProviderRepository.save(serviceProvider);
                break;
        }
    }

    private void processGroup(List<String> values) {
        Group group = new Group();
        group.setId(Integer.parseInt(values.get(0)));
        group.setName(values.get(1));
        group.setDescription(values.get(2));
        groupRepository.save(group);
    }

    private void processOpportunity(List<String> values) {
        Opportunity opportunity = new Opportunity();
        opportunity.setId(Integer.parseInt(values.get(0)));
        opportunity.setName(values.get(1));
        opportunity.setDescription(values.get(2));
        opportunity.setLocation(values.get(3));
        opportunity.setStart(values.get(4));
        opportunity.setEnd(values.get(5));
        opportunityRepository.save(opportunity);
    }

    private void processStory(List<String> values) {
        Story story = new Story();
        story.setId(Integer.parseInt(values.get(0)));
        story.setTitle(values.get(1));
        story.setContent(values.get(2));
        storyRepository.save(story);
    }

    private void processMemberships(List<String> values) {
        User user = userRepository.findById(Integer.parseInt(values.get(0))).get();
        Group group = groupRepository.findById(Integer.parseInt(values.get(1))).get();
        user.addGroup(group);
        userRepository.save(user);
    }

    private void processSharings(List<String> values) {
        Group group = groupRepository.findById(Integer.parseInt(values.get(1))).get();
        Story story = storyRepository.findById(Integer.parseInt(values.get(2))).get();
        group.addStory(story);
        groupRepository.save(group);
    }

    private void iterate(List<String> list, Object obj) {
        for (String tuple : list) {
            List<String> values = Arrays.asList(tuple.split(";"));

            if (obj.equals(User.class)) {
                processUser(values);
            } else if (obj.equals(Group.class)) {
                processGroup(values);
            } else if (obj.equals(Opportunity.class)) {
                processOpportunity(values);
            } else if (obj.equals(Story.class)) {
                processStory(values);
            }  else if (obj.equals("memberships")) {
                processMemberships(values);
            } else if (obj.equals("sharings")) {
                processSharings(values);
            }
        }
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Adding users (" + User.class + ")");
        iterate(users, User.class);
        logger.info("Adding groups (" + User.class + ")");
        iterate(groups, Group.class);
        logger.info("Adding opportunities (" + User.class + ")");
        iterate(opportunities, Opportunity.class);
        logger.info("Adding stories (" + User.class + ")");
        iterate(stories, Story.class);
        /*logger.info("Adding memberships ()");
        iterate(memberships, "memberships");
        logger.info("Adding sharings ()");
        iterate(sharings, "sharings");*/
    }
}
