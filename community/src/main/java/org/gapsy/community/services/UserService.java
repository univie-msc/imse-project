package org.gapsy.community.services;

import org.gapsy.community.models.ServiceProvider;
import org.gapsy.community.models.Student;
import org.gapsy.community.models.User;
import org.gapsy.community.repositories.ServiceProviderRepository;
import org.gapsy.community.repositories.StudentRepository;
import org.gapsy.community.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;
    private StudentRepository studentRepository;
    private ServiceProviderRepository serviceProviderRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       StudentRepository studentRepository,
                       ServiceProviderRepository serviceProviderRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.studentRepository = studentRepository;
        this.serviceProviderRepository = serviceProviderRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        user = userRepository.save(user);

        String role = user.getRole();

        if (role.equals("student")) {
            Student student = new Student();
            student.setUserId(user.getId());
            student.setSchoolName(""); // TODO
            student.setExpectedGraduationDate("2020-01-01"); // TODO
            studentRepository.save(student);
        } else if (role.equals("serviceProvider")) {
            ServiceProvider serviceProvider = new ServiceProvider();
            serviceProvider.setUserId(user.getId());
            serviceProvider.setCompanyName(""); // TODO
            serviceProvider.setBusinessAddress(""); // TODO
            serviceProviderRepository.save(serviceProvider);
        }

        return user;
    }
}
