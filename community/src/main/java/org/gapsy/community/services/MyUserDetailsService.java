package org.gapsy.community.services;

import org.gapsy.community.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.findUserByEmail(email);

        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        if (user.getAdministrator() != null) {
            authorities.add(new SimpleGrantedAuthority("ADMIN"));
        }

        if (user.getServiceProvider() != null) {
            authorities.add(new SimpleGrantedAuthority("SERVICE_PROVIDER"));
        }

        if (user.getStudent() != null) {
            authorities.add(new SimpleGrantedAuthority("STUDENT"));
        }

        return buildUserForAuthentication(user, authorities);
    }

    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), true,
                true, true, true, authorities);
    }
}
