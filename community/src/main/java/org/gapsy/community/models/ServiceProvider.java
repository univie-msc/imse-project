package org.gapsy.community.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "service_provider")
public class ServiceProvider extends BaseModel {
    @Id
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "business_address")
    private String businessAddress;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany
    @JoinTable(
            name = "advertisement",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "opportunity_id")
    )
    private Set<Opportunity> opportunities;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Opportunity> getOpportunities() {
        return opportunities;
    }

    public void addOpportunity(Opportunity opportunity) {
        opportunities.add(opportunity);
    }
}
