package org.gapsy.community.models;

import javax.persistence.*;

@Entity
public class Biography extends BaseModel {
    @Id
    @Column(name = "user_id")
    private Integer userId;

    private String info;

    private String hobby;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
