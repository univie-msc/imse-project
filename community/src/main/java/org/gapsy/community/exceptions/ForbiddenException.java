package org.gapsy.community.exceptions;

public class ForbiddenException extends RuntimeException {
    public ForbiddenException() {
        super("You are not allowed to view this content.");
    }
}