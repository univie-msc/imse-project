package org.gapsy.community.repositories;

import org.gapsy.community.models.ServiceProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceProviderRepository extends CrudRepository<ServiceProvider, Integer> {
}
