package org.gapsy.community.repositories;

import org.gapsy.community.models.Biography;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BiographyRepository extends CrudRepository<Biography, Integer> {
}
