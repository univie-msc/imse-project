-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.27 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table imse_gapsy_community.administrator
CREATE TABLE IF NOT EXISTS `administrator` (
  `user_id` int(10) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `emergency_contact_phone` varchar(50) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `FK_administrator_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.advertisement
CREATE TABLE IF NOT EXISTS `advertisement` (
  `user_id` int(10) unsigned NOT NULL,
  `opportunity_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `user_id_opportunity_id` (`user_id`,`opportunity_id`),
  KEY `FK_advertisement_opportunity` (`opportunity_id`),
  CONSTRAINT `FK_advertisement_opportunity` FOREIGN KEY (`opportunity_id`) REFERENCES `opportunity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_advertisement_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.application
CREATE TABLE IF NOT EXISTS `application` (
  `user_id` int(10) unsigned NOT NULL,
  `opportunity_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `user_id_opportunity_id` (`user_id`,`opportunity_id`),
  KEY `FK_application_opportunity` (`opportunity_id`),
  CONSTRAINT `FK_application_opportunity` FOREIGN KEY (`opportunity_id`) REFERENCES `opportunity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_application_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.biography
CREATE TABLE IF NOT EXISTS `biography` (
  `user_id` int(10) unsigned NOT NULL,
  `info` text NOT NULL,
  `hobby` text NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `FK_biography_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.following
CREATE TABLE IF NOT EXISTS `following` (
  `follower_user_id` int(10) unsigned NOT NULL,
  `followed_user_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `follower_user_id_followed_user_id` (`follower_user_id`,`followed_user_id`),
  KEY `FK_following_user_2` (`followed_user_id`),
  CONSTRAINT `FK_following_user` FOREIGN KEY (`follower_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_following_user_2` FOREIGN KEY (`followed_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.group
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.membership
CREATE TABLE IF NOT EXISTS `membership` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `user_id_group_id` (`user_id`,`group_id`),
  KEY `FK_membership_group` (`group_id`),
  CONSTRAINT `FK_membership_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_membership_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.opportunity
CREATE TABLE IF NOT EXISTS `opportunity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.service_provider
CREATE TABLE IF NOT EXISTS `service_provider` (
  `user_id` int(10) unsigned NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `business_address` varchar(255) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `FK_service_provider_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.sharing
CREATE TABLE IF NOT EXISTS `sharing` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `story_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `user_id_group_id_story_id` (`user_id`,`group_id`,`story_id`),
  KEY `FK_sharing_story` (`story_id`),
  CONSTRAINT `FK_sharing_membership` FOREIGN KEY (`user_id`, `group_id`) REFERENCES `membership` (`user_id`, `group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_sharing_story` FOREIGN KEY (`story_id`) REFERENCES `story` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.story
CREATE TABLE IF NOT EXISTS `story` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.student
CREATE TABLE IF NOT EXISTS `student` (
  `user_id` int(10) unsigned NOT NULL,
  `school_name` varchar(255) NOT NULL,
  `expected_graduation_date` date NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `FK_student_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table imse_gapsy_community.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
