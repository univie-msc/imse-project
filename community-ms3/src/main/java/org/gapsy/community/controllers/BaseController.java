package org.gapsy.community.controllers;

import org.gapsy.community.models.mongodb.User;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public abstract class BaseController {
    @Autowired
    private UserRepo userRepository;

    public User getCurrentUser(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        return userRepository.findByEmail(principal.getName());
    }
}
