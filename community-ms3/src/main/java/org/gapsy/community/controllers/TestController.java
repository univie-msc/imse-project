package org.gapsy.community.controllers;

import org.gapsy.community.models.mongodb.User;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/test")
public class TestController extends BaseController {

    @Autowired
    private UserRepo userRepo;

    @GetMapping("/")
    public String addStory() {
        Iterable<User> users = userRepo.findAll();

        return "TEST";
    }
}
