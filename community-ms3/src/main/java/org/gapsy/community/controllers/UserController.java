package org.gapsy.community.controllers;

import org.gapsy.community.models.mongodb.User;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path="/user")
public class UserController extends BaseController {
    @Autowired
    private UserRepo userRepository;

    @GetMapping(path="/all")
    public ModelAndView all() {
        Iterable<User> users = userRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("user-list");
        modelAndView.addObject("users", users);

        return modelAndView;
    }
}
