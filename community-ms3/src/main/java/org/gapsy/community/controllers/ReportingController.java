package org.gapsy.community.controllers;

import org.gapsy.community.models.mongodb.Opportunity;
import org.gapsy.community.models.mongodb.User;
import org.gapsy.community.repositories.mongodb.OpportunityRepo;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(path="/reporting")
public class ReportingController extends BaseController {
    @Autowired
    private OpportunityRepo opportunityRepository;
    @Autowired
    private UserRepo userRepository;

    @GetMapping("/student-applications")
    public ModelAndView studentApplications() {
        Iterable<Opportunity> opportunities = opportunityRepository.findAll();

        for (Opportunity opportunity : opportunities) {
            opportunity.students = userRepository.findStudentsByOpporunityId(opportunity.getId());
        }

        ModelAndView modelAndView = new ModelAndView("student-applications");
        modelAndView.addObject("opportunities", opportunities);

        return modelAndView;
    }
}
