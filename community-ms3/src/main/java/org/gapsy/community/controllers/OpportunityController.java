package org.gapsy.community.controllers;

import org.gapsy.community.exceptions.ForbiddenException;
import org.gapsy.community.exceptions.NotFoundException;
import org.gapsy.community.models.mongodb.Opportunity;
import org.gapsy.community.models.mongodb.Student;
import org.gapsy.community.models.mongodb.User;
import org.gapsy.community.repositories.mongodb.OpportunityRepo;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(path="/opportunity")
public class OpportunityController extends BaseController {
    @Autowired
    private OpportunityRepo opportunityRepository;

    @Autowired
    private UserRepo userRepository;

    @GetMapping("/all")
    public ModelAndView all(HttpServletRequest request) {
        Iterable<Opportunity> opportunities = opportunityRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("opportunity-list");
        modelAndView.addObject("opportunities", opportunities);
        modelAndView.addObject("currentUser", getCurrentUser(request));

        return modelAndView;
    }

    @RequestMapping(value="/advertise", method = RequestMethod.GET)
    public ModelAndView advertise() {
        ModelAndView modelAndView = new ModelAndView();
        Opportunity opportunity = new Opportunity();
        modelAndView.addObject("opportunity", opportunity);
        modelAndView.setViewName("advertise");

        return modelAndView;
    }

    @RequestMapping(value="/advertise", method = RequestMethod.POST)
    public String save(@Valid Opportunity opportunity, HttpServletRequest request) {
        User user = getCurrentUser(request);

        opportunity.setAdvertisedBy(user.getId());
        opportunity = opportunityRepository.save(opportunity);

        user.getServiceProvider().getAdvertisements().add(opportunity.getId());
        userRepository.save(user);

        return "redirect:all";
    }

    @RequestMapping(value = "apply", method = RequestMethod.POST)
    public String apply(@RequestParam String id, HttpServletRequest request) {
        Opportunity opportunity = opportunityRepository.findById(id).orElseThrow(() -> new NotFoundException());

        User user = getCurrentUser(request);
        Student student = user.getStudent();

        if (student == null) {
            throw new ForbiddenException();
        }

        if (!opportunity.hasApplied(user)) {
            opportunity.getApplicants().add(user.getId());
            opportunityRepository.save(opportunity);

            user.getStudent().getApplications().add(opportunity.getId());
            userRepository.save(user);
        }

        return "redirect:all";
    }
}
