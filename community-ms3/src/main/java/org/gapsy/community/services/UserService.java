package org.gapsy.community.services;

import org.gapsy.community.models.mongodb.ServiceProvider;
import org.gapsy.community.models.mongodb.Student;
import org.gapsy.community.models.mongodb.User;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepo userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepo userRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        if (user.getRole().equals("student")) {
            user.setStudent(new Student("", "2020-01-01")); // TODO
        } else if (user.getRole().equals("serviceProvider")) {
            user.setServiceProvider(new ServiceProvider("", "")); // TODO
        }

        user = userRepository.save(user);

        return user;
    }
}
