package org.gapsy.community.models.mongodb;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Document
public class User {
    @Id
    private ObjectId id;

    @Indexed(unique = true)
    private String email;

    private String password;

    private String firstname;

    private String lastname;

    private Biography biography;

    private Administrator administrator;

    private ServiceProvider serviceProvider;

    private Student student;

    private List<ObjectId> groups;

    private List<ObjectId> followingUsers;

    private List<ObjectId> followerUsers;

    @Transient
    private String role = "";

    public User() {
        groups = new ArrayList<>();
    }

    public User(String email,
                String password,
                String firstname,
                String lastname,
                Biography biography,
                Administrator administrator, // only one can be set
                ServiceProvider serviceProvider, // only one can be set
                Student student) { // only one can be set
        super();

        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.biography = biography;
        this.administrator = administrator;
        this.serviceProvider = serviceProvider;
        this.student = student;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Biography getBiography() {
        return biography;
    }

    public void setBiography(Biography biography) {
        this.biography = biography;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<ObjectId> getGroups() {
        return groups;
    }

    public void setGroups(List<ObjectId> groups) {
        this.groups = groups;
    }

    public List<ObjectId> getFollowingUsers() {
        return followingUsers;
    }

    public void setFollowingUsers(List<ObjectId> followingUsers) {
        this.followingUsers = followingUsers;
    }

    public List<ObjectId> getFollowerUsers() {
        return followerUsers;
    }

    public void setFollowerUsers(List<ObjectId> followerUsers) {
        this.followerUsers = followerUsers;
    }
}
