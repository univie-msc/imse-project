package org.gapsy.community.models.mysql;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "`group`")
public class Group extends BaseModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "groups")
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "sharing",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "story_id")
    )
    private Set<Story> stories;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<User> getUsers() { return users; }

    public Set<Story> getStories() {
        return stories;
    }

    public boolean isMember(User user) {
        return getUsers().contains(user);
    }

    public void addStory(Story story) {
        stories.add(story);
    }
}
