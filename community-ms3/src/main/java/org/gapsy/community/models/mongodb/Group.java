package org.gapsy.community.models.mongodb;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class Group {
    @Id
    private ObjectId id;

    private String name;

    private String description;

    private List<Story> stories;

    private List<ObjectId> users;

    public Group() {
        stories = new ArrayList<>();
        users = new ArrayList<>();
    }

    public Group(String name, String description, List<Story> stories) {
        super();

        this.name = name;
        this.description = description;
        this.stories = stories;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Story> getStories() {
        return stories;
    }

    public void setStories(List<Story> stories) {
        this.stories = stories;
    }

    public List<ObjectId> getUsers() {
        return users;
    }

    public void setUsers(List<ObjectId> users) {
        this.users = users;
    }

    public boolean isMember(User user) {
        return users.contains(user.getId());
    }

    public void addStory(Story story) {
        stories.add(story);
    }
}
