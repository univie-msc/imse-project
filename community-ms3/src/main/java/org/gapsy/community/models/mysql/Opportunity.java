package org.gapsy.community.models.mysql;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Opportunity extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    private String location;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String start;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String end;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "opportunities")
    private Set<ServiceProvider> serviceProviders;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "opportunities")
    private Set<Student> students;

    public Opportunity() {
        serviceProviders = new HashSet<ServiceProvider>();
        students = new HashSet<Student>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Set<ServiceProvider> getServiceProviders() {
        return serviceProviders;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public boolean hasApplied(Student student) {
        return students.contains(student);
    }




    /*public void addServiceProvider(ServiceProvider serviceProvider) {
        serviceProviders.add(serviceProvider);
    }*/
}
