package org.gapsy.community.models.mongodb;

import groovy.transform.ToString;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

@ToString
public class ServiceProvider {
    private String companyName;

    private String businessAddress;

    private List<ObjectId> advertisements;

    private ServiceProvider() {
        advertisements = new ArrayList<>();
    }

    public ServiceProvider(String companyName,
                           String businessAddress) {
        super();

        this.companyName = companyName;
        this.businessAddress = businessAddress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public List<ObjectId> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<ObjectId> advertisements) {
        this.advertisements = advertisements;
    }
}
