package org.gapsy.community.models.mongodb;

import groovy.transform.ToString;

@ToString
public class Administrator {
    private int level;

    private String emergencyContactPhone;

    public Administrator() { }

    public Administrator(int level, String emergencyContactPhone) {
        this.level = level;
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }
}
