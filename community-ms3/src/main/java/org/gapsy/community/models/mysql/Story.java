package org.gapsy.community.models.mysql;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Story extends BaseModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private String content;

    @ManyToMany(mappedBy = "stories")
    private Set<User> users;

    public Story() {
        users = new HashSet<User>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<User> getUsers() {
        return users;
    }

    /*public User getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(User sharedBy) {
        this.sharedBy = sharedBy;
    }*/

    public void addUser(User user) {
        users.add(user);
    }
}
