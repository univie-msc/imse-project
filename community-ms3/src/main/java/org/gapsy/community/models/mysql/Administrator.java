package org.gapsy.community.models.mysql;

import javax.persistence.*;

@Entity
public class Administrator extends BaseModel {
    @Id
    @Column(name = "user_id")
    private Integer userId;

    private Integer level;

    @Column(name = "emergency_contact_phone")
    private String emergencyContactPhone;

    @OneToOne()
    @JoinColumn(name = "user_id")
    private User user;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
