package org.gapsy.community.models.mongodb;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class Opportunity {
    @Id
    private ObjectId id;

    private String name;

    private String description;

    private String location;

    private String start;

    private String end;

    private ObjectId advertisedBy;

    private List<ObjectId> applicants;

    @Transient
    public List<User> students; // This is filled from outside

    public Opportunity() {
        applicants = new ArrayList<>();
    }

    public Opportunity(String name,
                       String description,
                       String location,
                       String start,
                       String end,
                       ObjectId advertisedBy) {
        super();

        this.name = name;
        this.description = description;
        this.location = location;
        this.start = start;
        this.end = end;
        this.advertisedBy = advertisedBy;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public ObjectId getAdvertisedBy() {
        return advertisedBy;
    }

    public void setAdvertisedBy(ObjectId advertisedBy) {
        this.advertisedBy = advertisedBy;
    }

    public List<ObjectId> getApplicants() {
        return applicants;
    }

    public void setApplicants(List<ObjectId> applicants) {
        this.applicants = applicants;
    }

    public boolean hasApplied(User studentUser) {
        return applicants.contains(studentUser.getId());
    }

    /*public List<User> getStudents() {
        return userRepo.findStudentsByOpporunityId(this.getId());
    }*/
}
