package org.gapsy.community.models.mysql;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Student extends BaseModel {
    @Id
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "school_name")
    private String schoolName;

    @Column(name = "expected_graduation_date")
    private String expectedGraduationDate;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "application",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "opportunity_id")
    )
    private Set<Opportunity> opportunities;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getExpectedGraduationDate() {
        return expectedGraduationDate;
    }

    public void setExpectedGraduationDate(String expectedGraduationDate) {
        this.expectedGraduationDate = expectedGraduationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Opportunity> getOpportunities() {
        return opportunities;
    }

    public void addOpportunity(Opportunity opportunity) {
        opportunities.add(opportunity);
    }
}
