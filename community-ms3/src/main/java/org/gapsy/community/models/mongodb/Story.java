package org.gapsy.community.models.mongodb;

import groovy.transform.ToString;
import org.bson.types.ObjectId;

@ToString
public class Story {
    private String title;

    private String content;

    private ObjectId userId;

    public Story() {}

    public Story(String title,
                 String content,
                 ObjectId userId) {
        this.title = title;
        this.content = content;
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }
}
