package org.gapsy.community.models.mongodb;

import groovy.transform.ToString;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

@ToString
public class Student {
    private String schoolName;

    private String expectedGraduationDate;

    private List<ObjectId> applications;

    public Student() {
        applications = new ArrayList<>();
    }

    public Student(String schoolName,
                   String expectedGraduationDate) {
        super();

        this.schoolName = schoolName;
        this.expectedGraduationDate = expectedGraduationDate;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getExpectedGraduationDate() {
        return expectedGraduationDate;
    }

    public void setExpectedGraduationDate(String expectedGraduationDate) {
        this.expectedGraduationDate = expectedGraduationDate;
    }

    public List<ObjectId> getApplications() {
        return applications;
    }

    public void setApplications(List<ObjectId> applications) {
        this.applications = applications;
    }
}
