package org.gapsy.community.models.mongodb;

import groovy.transform.ToString;

@ToString
public class Biography {
    private String info;

    private String hobby;

    public Biography() { }

    public Biography(String info, String hobby) {
        this.info = info;
        this.hobby = hobby;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
}
