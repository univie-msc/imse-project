package org.gapsy.community.models.mysql;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User extends BaseModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String email;

    private String password;

    private String firstname;

    private String lastname;

    @Transient
    private String role = "";

    @OneToOne(mappedBy = "user")
    @JsonIgnore
    private Biography biography;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "membership",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    @JsonIgnore
    private Set<Group> groups;

    @ManyToMany
    @JoinTable(
            name = "sharing",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "story_id")
    )
    @JsonIgnore
    private Set<Story> stories;

    @ManyToMany
    @JoinTable(
            name = "following",
            joinColumns = @JoinColumn(name = "follower_user_id"),
            inverseJoinColumns = @JoinColumn(name = "followed_user_id")
    )
    @JsonIgnore
    private Set<User> followers;

    @ManyToMany
    @JoinTable(
            name = "following",
            joinColumns = @JoinColumn(name = "followed_user_id"),
            inverseJoinColumns = @JoinColumn(name = "follower_user_id")
    )
    @JsonIgnore
    private Set<User> followings;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "user")
    private Administrator administrator;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "user")
    private ServiceProvider serviceProvider;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "user")
    private Student student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Biography getBiography() {
        return biography;
    }

    public void setBiography(Biography biography) {
        this.biography = biography;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public Set<Story> getStories() {
        return stories;
    }

    public Set<User> getFollowers() {
        return followers;
    }

    public Set<User> getFollowings() {
        return followings;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public ServiceProvider getServiceProvider() {
        return serviceProvider;
    }

    public Student getStudent() {
        return student;
    }

    public void addGroup(Group group) {
        groups.add(group);
    }
}
