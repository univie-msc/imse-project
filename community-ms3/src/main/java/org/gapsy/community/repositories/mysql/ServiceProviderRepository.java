package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.ServiceProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceProviderRepository extends CrudRepository<ServiceProvider, Integer> {
}
