package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<Group, Integer> {
}
