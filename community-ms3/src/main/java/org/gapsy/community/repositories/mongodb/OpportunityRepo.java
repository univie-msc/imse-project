package org.gapsy.community.repositories.mongodb;

import org.gapsy.community.models.mongodb.Opportunity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OpportunityRepo extends MongoRepository<Opportunity, String> {
}
