package org.gapsy.community.repositories.mysql;

import org.springframework.data.repository.CrudRepository;

import org.gapsy.community.models.mysql.User;
import org.springframework.stereotype.Repository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);
}
