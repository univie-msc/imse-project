package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.Opportunity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OpportunityRepository extends CrudRepository<Opportunity, Integer> {
}
