package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.Biography;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BiographyRepository extends CrudRepository<Biography, Integer> {
}
