package org.gapsy.community.repositories.mongodb;

import org.bson.types.ObjectId;
import org.gapsy.community.models.mongodb.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface UserRepo extends MongoRepository<User, String> {
    public User findByEmail(String email);

    @Query("{ 'student.applications': ?0 }")
    public List<User> findStudentsByOpporunityId(ObjectId opportunityId);
}