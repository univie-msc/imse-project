package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.Story;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoryRepository extends CrudRepository<Story, Integer> {
}
