package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {
}
