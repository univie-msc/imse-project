package org.gapsy.community.repositories.mongodb;

import org.gapsy.community.models.mongodb.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupRepo extends MongoRepository<Group, String> {
}
