package org.gapsy.community.repositories.mysql;

import org.gapsy.community.models.mysql.Administrator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends CrudRepository<Administrator, Integer> {
}
