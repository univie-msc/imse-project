package org.gapsy.community;

import org.gapsy.community.models.mongodb.*;
import org.gapsy.community.repositories.mongodb.GroupRepo;
import org.gapsy.community.repositories.mongodb.OpportunityRepo;
import org.gapsy.community.repositories.mongodb.UserRepo;
import org.gapsy.community.repositories.mysql.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(2)
public class MigrationRunner implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdministratorRepository administratorRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ServiceProviderRepository serviceProviderRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private OpportunityRepository opportunityRepository;
    @Autowired
    private StoryRepository storyRepository;

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private OpportunityRepo opportunityRepo;
    @Autowired
    private GroupRepo groupRepo;

    private void dropCollections() {
        opportunityRepo.deleteAll();
        groupRepo.deleteAll();
        userRepo.deleteAll();
    }

    private void migrateUsers() {
        Iterable<org.gapsy.community.models.mysql.User> rUsers = userRepository.findAll();

        for (org.gapsy.community.models.mysql.User rUser : rUsers) {
            Biography mBio = null;
            Administrator mAdmin = null;
            ServiceProvider mSP = null;
            Student mStudent = null;

            try {
                mBio = new Biography(rUser.getBiography().getInfo(),
                        rUser.getBiography().getHobby());
            } catch (NullPointerException ex) { }
            try {
                mAdmin = new Administrator(rUser.getAdministrator().getLevel(),
                        rUser.getAdministrator().getEmergencyContactPhone());
            } catch (NullPointerException ex) { }
            try {
                mSP = new ServiceProvider(rUser.getServiceProvider().getCompanyName(),
                        rUser.getServiceProvider().getBusinessAddress());
            } catch (NullPointerException ex) { }
            try {
                mStudent = new Student(rUser.getStudent().getSchoolName(),
                        rUser.getStudent().getExpectedGraduationDate());
            } catch (NullPointerException ex) { }

            User mUser = new User(rUser.getEmail(),
                    rUser.getPassword(),
                    rUser.getFirstname(),
                    rUser.getLastname(),
                    mBio,
                    mAdmin,
                    mSP,
                    mStudent
            );

            /*
            // ADD ADVERTISEMENTS
            if (rUser.getServiceProvider() != null) {
                for (org.gapsy.community.models.mysql.Opportunity opp :
                        rUser.getServiceProvider().getOpportunities()) {
                    mUser.getServiceProvider().getAdvertisements().add(opp.getId().toString());
                }
            }

            // ADD APPLICATIONS
            if (rUser.getStudent() != null) {
                for (org.gapsy.community.models.mysql.Opportunity opp :
                        rUser.getStudent().getOpportunities()) {
                    mUser.getStudent().getApplications().add(opp.getId().toString());
                }
            }

            // ADD GROUPS
            for (org.gapsy.community.models.mysql.Group group : rUser.getGroups()) {
                mUser.getGroups().add(group.getId().toString());
            }

            mUser.setId(rUser.getId().toString()); // Use ID from MySQL
            */

            userRepo.save(mUser);
        }
    }

    private void migrateGroups() {
        Iterable<org.gapsy.community.models.mysql.Group> rGroups = groupRepository.findAll();

        for (org.gapsy.community.models.mysql.Group rGroup : rGroups) {
            List<Story> mStories = new ArrayList<>();

            // ADD STORIES
            for (org.gapsy.community.models.mysql.Story rStory : rGroup.getStories()) {
                mStories.add(new Story(rStory.getTitle(), rStory.getContent(), null)); // TODO userId
            }

            Group mGroup = new Group(rGroup.getName(),
                    rGroup.getDescription(),
                    mStories
            );

            /*
            // ADD USERS
            for (org.gapsy.community.models.mysql.User rUser : rGroup.getUsers()) {
                mGroup.getUsers().add(rUser.getId().toString());
            }

            mGroup.setId(rGroup.getId().toString()); // Use ID from MySQL
            */

            groupRepo.save(mGroup);
        }
    }

    private void migrateOpportunities() {
        Iterable<org.gapsy.community.models.mysql.Opportunity> rOpportunities = opportunityRepository.findAll();

        for (org.gapsy.community.models.mysql.Opportunity rOpp : rOpportunities) {
            Opportunity mOpp = new Opportunity(rOpp.getName(),
                    rOpp.getDescription(),
                    rOpp.getLocation(),
                    rOpp.getStart(),
                    rOpp.getEnd(),
                    null // advertisedBy userID
            );

            /*
            // ADD APPLICANTS
            for (org.gapsy.community.models.mysql.Student rStd : rOpp.getStudents()) {
                mOpp.getApplicants().add(rStd.getUserId().toString());
            }

            mOpp.setId(rOpp.getId().toString()); // Use ID from MySQL
            */

            opportunityRepo.save(mOpp);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        // Prefixes: r - relational MySQL, m - MongoDB

        System.out.println("Migrating from MySQL to MongoDB...");

        System.out.println("Dropping collections...");
        dropCollections();
        System.out.println("Done.");

        System.out.println("Migrating users...");
        migrateUsers();
        System.out.println("Done.");
        System.out.println("Migrating groups...");
        migrateGroups();
        System.out.println("Done.");
        System.out.println("Migrating opportunities...");
        migrateOpportunities();
        System.out.println("Done.");
    }
}
