package org.gapsy.community.exceptions;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("Element not found");
    }
}
